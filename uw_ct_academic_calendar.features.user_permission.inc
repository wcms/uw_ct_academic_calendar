<?php

/**
 * @file
 * uw_ct_academic_calendar.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_academic_calendar_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'admin gsac'.
  $permissions['admin gsac'] = array(
    'name' => 'admin gsac',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_academic_calendar',
  );

  // Exported permission: 'create field_course_academic_level'.
  $permissions['create field_course_academic_level'] = array(
    'name' => 'create field_course_academic_level',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_catalog_number'.
  $permissions['create field_course_catalog_number'] = array(
    'name' => 'create field_course_catalog_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_component'.
  $permissions['create field_course_component'] = array(
    'name' => 'create field_course_component',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_cross_listings'.
  $permissions['create field_course_cross_listings'] = array(
    'name' => 'create field_course_cross_listings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_description'.
  $permissions['create field_course_description'] = array(
    'name' => 'create field_course_description',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_faculty'.
  $permissions['create field_course_faculty'] = array(
    'name' => 'create field_course_faculty',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_id'.
  $permissions['create field_course_id'] = array(
    'name' => 'create field_course_id',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_requisites'.
  $permissions['create field_course_requisites'] = array(
    'name' => 'create field_course_requisites',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_subject'.
  $permissions['create field_course_subject'] = array(
    'name' => 'create field_course_subject',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_topic_titles'.
  $permissions['create field_course_topic_titles'] = array(
    'name' => 'create field_course_topic_titles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_course_units'.
  $permissions['create field_course_units'] = array(
    'name' => 'create field_course_units',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_gsac_plan_code'.
  $permissions['create field_gsac_plan_code'] = array(
    'name' => 'create field_gsac_plan_code',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_academic_calendar content'.
  $permissions['create uw_academic_calendar content'] = array(
    'name' => 'create uw_academic_calendar content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_graduate_course content'.
  $permissions['create uw_graduate_course content'] = array(
    'name' => 'create uw_graduate_course content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_academic_calendar content'.
  $permissions['delete any uw_academic_calendar content'] = array(
    'name' => 'delete any uw_academic_calendar content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_graduate_course content'.
  $permissions['delete any uw_graduate_course content'] = array(
    'name' => 'delete any uw_graduate_course content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_academic_calendar content'.
  $permissions['delete own uw_academic_calendar content'] = array(
    'name' => 'delete own uw_academic_calendar content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_graduate_course content'.
  $permissions['delete own uw_graduate_course content'] = array(
    'name' => 'delete own uw_graduate_course content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in gsac_admit_term'.
  $permissions['delete terms in gsac_admit_term'] = array(
    'name' => 'delete terms in gsac_admit_term',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_application_materials'.
  $permissions['delete terms in gsac_application_materials'] = array(
    'name' => 'delete terms in gsac_application_materials',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_delivery_mode'.
  $permissions['delete terms in gsac_delivery_mode'] = array(
    'name' => 'delete terms in gsac_delivery_mode',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_milestone_name'.
  $permissions['delete terms in gsac_milestone_name'] = array(
    'name' => 'delete terms in gsac_milestone_name',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_program'.
  $permissions['delete terms in gsac_program'] = array(
    'name' => 'delete terms in gsac_program',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_program_type'.
  $permissions['delete terms in gsac_program_type'] = array(
    'name' => 'delete terms in gsac_program_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_registration'.
  $permissions['delete terms in gsac_registration'] = array(
    'name' => 'delete terms in gsac_registration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_study'.
  $permissions['delete terms in gsac_study'] = array(
    'name' => 'delete terms in gsac_study',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in gsac_term_year'.
  $permissions['delete terms in gsac_term_year'] = array(
    'name' => 'delete terms in gsac_term_year',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_academic_calendar content'.
  $permissions['edit any uw_academic_calendar content'] = array(
    'name' => 'edit any uw_academic_calendar content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_graduate_course content'.
  $permissions['edit any uw_graduate_course content'] = array(
    'name' => 'edit any uw_graduate_course content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_course_academic_level'.
  $permissions['edit field_course_academic_level'] = array(
    'name' => 'edit field_course_academic_level',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_catalog_number'.
  $permissions['edit field_course_catalog_number'] = array(
    'name' => 'edit field_course_catalog_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_component'.
  $permissions['edit field_course_component'] = array(
    'name' => 'edit field_course_component',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_cross_listings'.
  $permissions['edit field_course_cross_listings'] = array(
    'name' => 'edit field_course_cross_listings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_description'.
  $permissions['edit field_course_description'] = array(
    'name' => 'edit field_course_description',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_faculty'.
  $permissions['edit field_course_faculty'] = array(
    'name' => 'edit field_course_faculty',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_id'.
  $permissions['edit field_course_id'] = array(
    'name' => 'edit field_course_id',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_requisites'.
  $permissions['edit field_course_requisites'] = array(
    'name' => 'edit field_course_requisites',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_subject'.
  $permissions['edit field_course_subject'] = array(
    'name' => 'edit field_course_subject',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_topic_titles'.
  $permissions['edit field_course_topic_titles'] = array(
    'name' => 'edit field_course_topic_titles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_course_units'.
  $permissions['edit field_course_units'] = array(
    'name' => 'edit field_course_units',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_gsac_plan_code'.
  $permissions['edit field_gsac_plan_code'] = array(
    'name' => 'edit field_gsac_plan_code',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_academic_level'.
  $permissions['edit own field_course_academic_level'] = array(
    'name' => 'edit own field_course_academic_level',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_catalog_number'.
  $permissions['edit own field_course_catalog_number'] = array(
    'name' => 'edit own field_course_catalog_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_component'.
  $permissions['edit own field_course_component'] = array(
    'name' => 'edit own field_course_component',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_cross_listings'.
  $permissions['edit own field_course_cross_listings'] = array(
    'name' => 'edit own field_course_cross_listings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_description'.
  $permissions['edit own field_course_description'] = array(
    'name' => 'edit own field_course_description',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_faculty'.
  $permissions['edit own field_course_faculty'] = array(
    'name' => 'edit own field_course_faculty',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_id'.
  $permissions['edit own field_course_id'] = array(
    'name' => 'edit own field_course_id',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_requisites'.
  $permissions['edit own field_course_requisites'] = array(
    'name' => 'edit own field_course_requisites',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_subject'.
  $permissions['edit own field_course_subject'] = array(
    'name' => 'edit own field_course_subject',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_topic_titles'.
  $permissions['edit own field_course_topic_titles'] = array(
    'name' => 'edit own field_course_topic_titles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_course_units'.
  $permissions['edit own field_course_units'] = array(
    'name' => 'edit own field_course_units',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_gsac_plan_code'.
  $permissions['edit own field_gsac_plan_code'] = array(
    'name' => 'edit own field_gsac_plan_code',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_academic_calendar content'.
  $permissions['edit own uw_academic_calendar content'] = array(
    'name' => 'edit own uw_academic_calendar content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_graduate_course content'.
  $permissions['edit own uw_graduate_course content'] = array(
    'name' => 'edit own uw_graduate_course content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in gsac_admit_term'.
  $permissions['edit terms in gsac_admit_term'] = array(
    'name' => 'edit terms in gsac_admit_term',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_application_materials'.
  $permissions['edit terms in gsac_application_materials'] = array(
    'name' => 'edit terms in gsac_application_materials',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_delivery_mode'.
  $permissions['edit terms in gsac_delivery_mode'] = array(
    'name' => 'edit terms in gsac_delivery_mode',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_milestone_name'.
  $permissions['edit terms in gsac_milestone_name'] = array(
    'name' => 'edit terms in gsac_milestone_name',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_program'.
  $permissions['edit terms in gsac_program'] = array(
    'name' => 'edit terms in gsac_program',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_program_type'.
  $permissions['edit terms in gsac_program_type'] = array(
    'name' => 'edit terms in gsac_program_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_registration'.
  $permissions['edit terms in gsac_registration'] = array(
    'name' => 'edit terms in gsac_registration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_study'.
  $permissions['edit terms in gsac_study'] = array(
    'name' => 'edit terms in gsac_study',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in gsac_term_year'.
  $permissions['edit terms in gsac_term_year'] = array(
    'name' => 'edit terms in gsac_term_year',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_academic_calendar revision log entry'.
  $permissions['enter uw_academic_calendar revision log entry'] = array(
    'name' => 'enter uw_academic_calendar revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar authored by option'.
  $permissions['override uw_academic_calendar authored by option'] = array(
    'name' => 'override uw_academic_calendar authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar authored on option'.
  $permissions['override uw_academic_calendar authored on option'] = array(
    'name' => 'override uw_academic_calendar authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar promote to front page option'.
  $permissions['override uw_academic_calendar promote to front page option'] = array(
    'name' => 'override uw_academic_calendar promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar published option'.
  $permissions['override uw_academic_calendar published option'] = array(
    'name' => 'override uw_academic_calendar published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar revision option'.
  $permissions['override uw_academic_calendar revision option'] = array(
    'name' => 'override uw_academic_calendar revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_academic_calendar sticky option'.
  $permissions['override uw_academic_calendar sticky option'] = array(
    'name' => 'override uw_academic_calendar sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_course_academic_level'.
  $permissions['view field_course_academic_level'] = array(
    'name' => 'view field_course_academic_level',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_catalog_number'.
  $permissions['view field_course_catalog_number'] = array(
    'name' => 'view field_course_catalog_number',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_component'.
  $permissions['view field_course_component'] = array(
    'name' => 'view field_course_component',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_cross_listings'.
  $permissions['view field_course_cross_listings'] = array(
    'name' => 'view field_course_cross_listings',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_description'.
  $permissions['view field_course_description'] = array(
    'name' => 'view field_course_description',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_faculty'.
  $permissions['view field_course_faculty'] = array(
    'name' => 'view field_course_faculty',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_id'.
  $permissions['view field_course_id'] = array(
    'name' => 'view field_course_id',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_requisites'.
  $permissions['view field_course_requisites'] = array(
    'name' => 'view field_course_requisites',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_subject'.
  $permissions['view field_course_subject'] = array(
    'name' => 'view field_course_subject',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_topic_titles'.
  $permissions['view field_course_topic_titles'] = array(
    'name' => 'view field_course_topic_titles',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_course_units'.
  $permissions['view field_course_units'] = array(
    'name' => 'view field_course_units',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_gsac_plan_code'.
  $permissions['view field_gsac_plan_code'] = array(
    'name' => 'view field_gsac_plan_code',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_academic_level'.
  $permissions['view own field_course_academic_level'] = array(
    'name' => 'view own field_course_academic_level',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_catalog_number'.
  $permissions['view own field_course_catalog_number'] = array(
    'name' => 'view own field_course_catalog_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_component'.
  $permissions['view own field_course_component'] = array(
    'name' => 'view own field_course_component',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_cross_listings'.
  $permissions['view own field_course_cross_listings'] = array(
    'name' => 'view own field_course_cross_listings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_description'.
  $permissions['view own field_course_description'] = array(
    'name' => 'view own field_course_description',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_faculty'.
  $permissions['view own field_course_faculty'] = array(
    'name' => 'view own field_course_faculty',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_id'.
  $permissions['view own field_course_id'] = array(
    'name' => 'view own field_course_id',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_requisites'.
  $permissions['view own field_course_requisites'] = array(
    'name' => 'view own field_course_requisites',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_subject'.
  $permissions['view own field_course_subject'] = array(
    'name' => 'view own field_course_subject',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_topic_titles'.
  $permissions['view own field_course_topic_titles'] = array(
    'name' => 'view own field_course_topic_titles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_course_units'.
  $permissions['view own field_course_units'] = array(
    'name' => 'view own field_course_units',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_gsac_plan_code'.
  $permissions['view own field_gsac_plan_code'] = array(
    'name' => 'view own field_gsac_plan_code',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
