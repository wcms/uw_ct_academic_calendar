<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php

      // Get the current month and year.
      $current_month = date('F');
      $current_year = date('Y');
      $current_term = '';

      // Based on current month to determine the current term.
      if ($current_month == 'January' || $current_month == 'February' || $current_month == 'March' || $current_month == 'April') {
        $current_term = 'Winter';
        $current_term_id = 1;
      }
      if ($current_month == 'May' || $current_month == 'June' || $current_month == 'July' || $current_month == 'August') {
        $current_term = 'Spring';
        $current_term_id = 2;
      }
      if ($current_month == 'September' || $current_month == 'October' || $current_month == 'November' || $current_month == 'December') {
        $current_term = 'Fall';
        $current_term_id = 3;
      }
      // Get the current term year. e.g. Winter 2016.
      $current_term_year = "";
      $current_term_year = $current_term . ' ' . $current_year;

      // Get the term and the year from the uw_ct_academic_calendar_settings_term_year variable under admin/config/system/gasc_settings. e.g. Winter 2016, Fall 2015.
      $current_term_year_variable = variable_get('uw_ct_academic_calendar_settings_term_year');
      if (isset($current_term_year_variable)) {
        $term = taxonomy_term_load($current_term_year_variable);
        $term_year_from_settings_variable = $term->name;
        $term_year = explode(" ", $term_year_from_settings_variable);
        $term = $term_year[0];
        $year = $term_year[1];

        if ($term == 'Winter') {
          $term_id = 1;
          $term_period = '(January 1, ' . $year . ' - April 30, ' . $year . ').';
        }
        elseif ($term == 'Spring') {
          $term_id = 2;
          $term_period = '(May 1, ' . $year . ' - August 31, ' . $year . ').';
        }
        else {
          $term_id = 3;
          $term_period = '(September 1, ' . $year . ' - December 31, ' . $year . ').';
        }

    $term_year_content = '<p>The Graduate Studies Academic Calendar is updated 3 times per year, at the start of each academic term (January 1, May 1, September 1).
          Graduate Studies Academic Calendars from previous terms can be found in the ' . l(t('archives'), 'archives') . '.</p>';

        $archive_more_content = ' This is the archived version; the most up-to-date program information is available through the <a href="https://uwaterloo.ca/graduate-studies-academic-calendar"> current Graduate Studies Academic Calendar</a>.';

        // If the term year selected under admin/config/system/gasc_settings is future year. e.g. current year is 2016, future year is 2017.
        if ($year > $current_year) {
          print('<p>The program information below will be valid for the <strong>' . strtolower($term_year_from_settings_variable) . ' term</strong> ' . $term_period . $term_year_content . '</p>');
        }

        // If the term year selected under admin/config/system/gasc_settings is past year. e.g. current year is 2016, past year is 2015.
        if ($year < $current_year) {
          print('<p>The program information below was valid for the <strong>' . strtolower($term_year_from_settings_variable) . ' term</strong> ' . $term_period . $archive_more_content . $term_year_content . '</p>');
        }

        // If the term year selected under admin/config/system/gasc_settings is current year, e.g. current year is 2016.
        if ($year == $current_year) {
          if ($term_id == $current_term_id) {
            print('<p>The program information below is valid for the <strong>' . strtolower($term_year_from_settings_variable) . ' term</strong> ' . $term_period . $term_year_content . '</p>');
          }

          if ($term_id > $current_term_id) {
            print('<p>The program information below will be valid for the <strong>' . strtolower($term_year_from_settings_variable) . ' term</strong> ' . $term_period . $term_year_content . '</p>');
          }

          if ($term_id < $current_term_id) {
            print('<p>The program information below was valid for the <strong>' . strtolower($term_year_from_settings_variable) . ' term</strong> ' . $term_period . $archive_more_content . $term_year_content . '</p>');
          }
        }
      }
    ?>

    <?php
      // Set $accelerated variable. If it is accelerated, set value as 1, otherwise set value 0.
      $accelerated = 1;
      $is_accelerated = $node->field_gsac_accelerated_program;
      if (empty($is_accelerated) || $is_accelerated[LANGUAGE_NONE][0]['value'] == 0) {
        $accelerated = 0;
      }
    ?>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);

      // Render accelerated detail field if it is accelerated.
      if ($accelerated == 1) {
        print render($content['field_gsac_accelerated_detail']);
      }
      // Render Foreword field if it is not accelerated.
      if ($accelerated == 0) {
        print render($content['field_gsac_foreword']);
      }
    ?>
    <?php
      // Print Program information, admission requirements, degree requirements, graduate research fields and graduate specializations fields if it is not accelerated.
      if ($accelerated == 0) {
    ?>
        <div class="program_node_anchor">
          <ul>
            <li><a href="#program_information">Program information</a></li>
            <li><a href="#admission_requirements">Admission requirements</a></li>
            <li><a href="#degree_requirements">Degree requirements</a></li>
          </ul>
        </div>

      <?php
        print render($content['field_gsac_research_area']);
        print render($content['field_gsac_graduate_spec']);
      ?>
    <?php
      }
    ?>

    <div class="field-name-field-gsac-program-markup">
      <?php if ($accelerated == 0) { ?>
          <h2><a name="program_information">Program information</a></h2>
      <?php } ?>
    </div>
    <ul class="program_information">
    <?php

      // Render the following fields if it is not accelerated.
      if ($accelerated == 0) {
        print render($content['field_gsac_admit_term']);
        print render($content['field_gsac_delivery_mode']);
        print render($content['field_gsac_delivery_info']);
        print render($content['field_gsac_length_program']);
        print render($content['field_gsac_program_type']);
        print render($content['field_gsac_registration_option']);
        print render($content['field_gsac_registration_info']);
        print render($content['field_gsac_study_option']);
        print render($content['field_gsac_addition_program']);
      }
    ?>
    </ul>

    <?php
      // Render the following fields if it is not accelerated.
      if ($accelerated == 0) {
    ?>
        <div class="field-name-field-gsac-admission-markup">
              <h2><a name="admission_requirements">Admission requirements</a></h2>
        </div>

        <ul class="admission_requirements">
          <?php
            print render($content['field_gsac_min_req']);
            print render($content['field_gsac_materials_sup']);
            print render($content['field_gsac_references']);
          ?>

          <?php
            if (!empty($node->field_gsac_elp_need) && $node->field_gsac_elp_need[LANGUAGE_NONE][0]['value'] == 1) {
          ?>
           <li class="field-label">
            <?php
              $elp = $node->field_gsac_elp;
              $elp_markup = $elp[LANGUAGE_NONE][0]['markup'];
              print($elp_markup);
            ?>
          </li>
         <?php
            }
          ?>
        </ul>

        <div class="field-name-field-gsac-degree-markup">
          <h2><a name="degree_requirements">Degree requirements</a></h2>
        </div>

        <ul class="degree_requirements">
          <?php
            print render($content['field_gsac_degree_req']);
          ?>
        </ul>
    <?php
      }
    ?>

    <?php
      // Render the below three fields no matter it is accelerated or not.
      print render($content['field_gsac_adds_list']);
      print render($content['field_gsac_dept_website']);
      print render($content['field_gsac_optional_website']);
      print render($content['field_gsac_plan_code']);
    ?>

  </div>
  <div class="return_to_results">
    <!-- when url includes 'program-search-result?faculty=', it displays the link Return to Results. -->
    <?php $pos = strpos($_SERVER['HTTP_REFERER'], 'graduate-program-search?faculty='); ?>
    <?php if ($pos !== FALSE): ?>
      <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">Return to Results</a>
    <?php endif;?>
  </div>
</div>
