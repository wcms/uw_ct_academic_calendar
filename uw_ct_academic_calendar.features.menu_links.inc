<?php

/**
 * @file
 * uw_ct_academic_calendar.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_academic_calendar_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_gsac-settings:admin/config/system/gasc_settings.
  $menu_links['menu-site-management_gsac-settings:admin/config/system/gasc_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/gasc_settings',
    'router_path' => 'admin/config/system/gasc_settings',
    'link_title' => 'GSAC Settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_gsac-settings:admin/config/system/gasc_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_import-graduate-courses:admin/config/system/import_graduate_courses.
  $menu_links['menu-site-management_import-graduate-courses:admin/config/system/import_graduate_courses'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/import_graduate_courses',
    'router_path' => 'admin/config/system/import_graduate_courses',
    'link_title' => 'Import graduate courses',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_import-graduate-courses:admin/config/system/import_graduate_courses',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-applied-health-sciences-page:admin/config/system/graduate_program/graduate_program_ahs_block.
  $menu_links['menu-site-management_manage-faculty-of-applied-health-sciences-page:admin/config/system/graduate_program/graduate_program_ahs_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_ahs_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_ahs_block',
    'link_title' => 'Manage Faculty of Health page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-applied-health-sciences-page:admin/config/system/graduate_program/graduate_program_ahs_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-arts-page:admin/config/system/graduate_program/graduate_program_arts_block.
  $menu_links['menu-site-management_manage-faculty-of-arts-page:admin/config/system/graduate_program/graduate_program_arts_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_arts_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_arts_block',
    'link_title' => 'Manage Faculty of Arts page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-arts-page:admin/config/system/graduate_program/graduate_program_arts_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-engineering-page:admin/config/system/graduate_program/graduate_program_eng_block.
  $menu_links['menu-site-management_manage-faculty-of-engineering-page:admin/config/system/graduate_program/graduate_program_eng_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_eng_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_eng_block',
    'link_title' => 'Manage Faculty of Engineering page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-engineering-page:admin/config/system/graduate_program/graduate_program_eng_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-environment-page:admin/config/system/graduate_program/graduate_program_env_block.
  $menu_links['menu-site-management_manage-faculty-of-environment-page:admin/config/system/graduate_program/graduate_program_env_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_env_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_env_block',
    'link_title' => 'Manage Faculty of Environment page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-environment-page:admin/config/system/graduate_program/graduate_program_env_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-mathematics-page:admin/config/system/graduate_program/graduate_program_math_block.
  $menu_links['menu-site-management_manage-faculty-of-mathematics-page:admin/config/system/graduate_program/graduate_program_math_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_math_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_math_block',
    'link_title' => 'Manage Faculty of Mathematics page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-mathematics-page:admin/config/system/graduate_program/graduate_program_math_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-faculty-of-science-page:admin/config/system/graduate_program/graduate_program_sci_block.
  $menu_links['menu-site-management_manage-faculty-of-science-page:admin/config/system/graduate_program/graduate_program_sci_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_sci_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_sci_block',
    'link_title' => 'Manage Faculty of Science page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-faculty-of-science-page:admin/config/system/graduate_program/graduate_program_sci_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-graduate-courses:admin/graduate-course-quick-report.
  $menu_links['menu-site-management_manage-graduate-courses:admin/graduate-course-quick-report'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/graduate-course-quick-report',
    'router_path' => 'admin/graduate-course-quick-report',
    'link_title' => 'Manage graduate courses',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-graduate-courses:admin/graduate-course-quick-report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-graduate-program-search-page:admin/config/system/graduate_program/graduate_program_search_header.
  $menu_links['menu-site-management_manage-graduate-program-search-page:admin/config/system/graduate_program/graduate_program_search_header'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_search_header',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_search_header',
    'link_title' => 'Manage graduate program search page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-graduate-program-search-page:admin/config/system/graduate_program/graduate_program_search_header',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_manage-theology-page:admin/config/system/graduate_program/graduate_program_theo_block.
  $menu_links['menu-site-management_manage-theology-page:admin/config/system/graduate_program/graduate_program_theo_block'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/graduate_program/graduate_program_theo_block',
    'router_path' => 'admin/config/system/graduate_program/graduate_program_theo_block',
    'link_title' => 'Manage Theology page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_manage-theology-page:admin/config/system/graduate_program/graduate_program_theo_block',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-admit-term-year:admin/structure/taxonomy/gsac_term_year.
  $menu_links['menu-site-manager-vocabularies_gsac-admit-term-year:admin/structure/taxonomy/gsac_term_year'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_term_year',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Admit Term Year',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-admit-term-year:admin/structure/taxonomy/gsac_term_year',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-admit-term:admin/structure/taxonomy/gsac_admit_term.
  $menu_links['menu-site-manager-vocabularies_gsac-admit-term:admin/structure/taxonomy/gsac_admit_term'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_admit_term',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Admit Term',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-admit-term:admin/structure/taxonomy/gsac_admit_term',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-application-materials:admin/structure/taxonomy/gsac_application_materials.
  $menu_links['menu-site-manager-vocabularies_gsac-application-materials:admin/structure/taxonomy/gsac_application_materials'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_application_materials',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Application Materials',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-application-materials:admin/structure/taxonomy/gsac_application_materials',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-delivery-mode:admin/structure/taxonomy/gsac_delivery_mode.
  $menu_links['menu-site-manager-vocabularies_gsac-delivery-mode:admin/structure/taxonomy/gsac_delivery_mode'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_delivery_mode',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Delivery Mode',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-delivery-mode:admin/structure/taxonomy/gsac_delivery_mode',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-faculty-department-program:admin/structure/taxonomy/gsac_program.
  $menu_links['menu-site-manager-vocabularies_gsac-faculty-department-program:admin/structure/taxonomy/gsac_program'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_program',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Faculty Department Program',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-faculty-department-program:admin/structure/taxonomy/gsac_program',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-milestone:admin/structure/taxonomy/gsac_milestone_name.
  $menu_links['menu-site-manager-vocabularies_gsac-milestone:admin/structure/taxonomy/gsac_milestone_name'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_milestone_name',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Milestone',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-milestone:admin/structure/taxonomy/gsac_milestone_name',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-program-type:admin/structure/taxonomy/gsac_program_type.
  $menu_links['menu-site-manager-vocabularies_gsac-program-type:admin/structure/taxonomy/gsac_program_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_program_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Program Type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-program-type:admin/structure/taxonomy/gsac_program_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-registration:admin/structure/taxonomy/gsac_registration.
  $menu_links['menu-site-manager-vocabularies_gsac-registration:admin/structure/taxonomy/gsac_registration'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_registration',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Registration',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-registration:admin/structure/taxonomy/gsac_registration',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_gsac-study-option:admin/structure/taxonomy/gsac_study.
  $menu_links['menu-site-manager-vocabularies_gsac-study-option:admin/structure/taxonomy/gsac_study'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/gsac_study',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'GSAC Study Option',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_gsac-study-option:admin/structure/taxonomy/gsac_study',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('GSAC Admit Term');
  t('GSAC Admit Term Year');
  t('GSAC Application Materials');
  t('GSAC Delivery Mode');
  t('GSAC Faculty Department Program');
  t('GSAC Milestone');
  t('GSAC Program Type');
  t('GSAC Registration');
  t('GSAC Settings');
  t('GSAC Study Option');
  t('Import graduate courses');
  t('Manage Faculty of Health page');
  t('Manage Faculty of Arts page');
  t('Manage Faculty of Engineering page');
  t('Manage Faculty of Environment page');
  t('Manage Faculty of Mathematics page');
  t('Manage Faculty of Science page');
  t('Manage Theology page');
  t('Manage graduate courses');
  t('Manage graduate program search page');

  return $menu_links;
}
