<?php

/**
 * @file
 * Install, update and uninstall functions for the uw_ct_award module.
 */

/**
 * Implements hook_install().
 *
 * Create a landing page (graduate-course-search) if there is no landing page
 * for graduate course search.
 */
function uw_ct_academic_calendar_install() {
  _uw_ct_academic_calendar_create_landing_page();
}

/**
 * Implements hook_enable().
 */
function uw_ct_academic_calendar_enable() {
  watchdog('uw_ct_academic_calendar', 'enable function executing');

  // Enable the view.
  // Read in any existing disabled views.
  $views_status = variable_get('views_defaults', array());

  // Remove ours from the list.
  unset($views_status['manage_graduate_programs']);
  unset($views_status['graduate_course_search']);
  unset($views_status['graduate_course_summary_block']);
  unset($views_status['graduate_courses']);
  unset($views_status['gsac_course_via_subject']);
  unset($views_status['gsac_mass_publish']);
  unset($views_status['gsac_report']);
  unset($views_status['gsac_search']);
  unset($views_status['gsac_services']);

  // Reset the variable with the new list.
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    // Clear the views cache.
    views_invalidate_cache();
  }

  // The landing page with alias awards/database will create automatically if it is not exist.
  _uw_ct_academic_calendar_create_landing_page();
}

/**
 * Implements hook_disable().
 */
function uw_ct_academic_calendar_disable() {
  watchdog('uw_ct_academic_calendar', 'disable function executing');

  // Disable the view.
  // Read in any existing disabled views.
  $views_status = variable_get('views_defaults', array());

  // Add ours to the list.
  $views_status['manage_graduate_programs'] = TRUE;
  $views_status['graduate_course_search'] = TRUE;
  $views_status['graduate_course_summary_block'] = TRUE;
  $views_status['graduate_courses'] = TRUE;
  $views_status['gsac_course_via_subject'] = TRUE;
  $views_status['gsac_mass_publish'] = TRUE;
  $views_status['gsac_report'] = TRUE;
  $views_status['gsac_search'] = TRUE;
  $views_status['gsac_services'] = TRUE;

  // Reset the variable with the new list.
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    // Clear the views cache.
    views_invalidate_cache();
  }

  // Remove headers menu link under Site management when the module is disabled.
}

/**
 * Returns an array with data for default pages.
 *
 * @param string $page
 *   The page data to return.
 */
function _uw_ct_academic_calendar_get_page_data($page) {
  switch ($page) {
    case 'graduate_course_search':
      return array(
        'title' => 'Graduate course search',
        'content' => 'This is your <em>graduate course</em> landing page.',
        'alias' => 'graduate-course-search',
        'weight' => 0,
      );
  }

  // No cases matched.
  return NULL;
}

/**
 * Create a landing page with alias graduate-course-search.
 *
 * The helper function to create a landing page with alias
 * graduate-course-search for graduate course search if it is not exist.
 */
function _uw_ct_academic_calendar_create_landing_page() {
  $path = ('graduate-course-search');
  $path_exist = drupal_lookup_path('alias', $path) || drupal_lookup_path('source', $path);

  if (empty($path_exist)) {
    watchdog('uw_ct_academic_calendar', 'graduate_course_search');
    $data = _uw_ct_academic_calendar_get_page_data('graduate_course_search');

    // Create the node.
    $node = new stdClass();
    $node->type = 'uw_web_page';
    node_object_prepare($node);
    $node->language = $GLOBALS['language_content']->language;

    // Published - yes.
    $node->status = 1;
    $node->title = $data['title'];
    $node->body[$node->language][0]['value'] = $data['content'];
    $node->body[$node->language][0]['format'] = 'uw_tf_standard';
    $node->path['alias'] = $data['alias'];
    $node->path['pathauto'] = 0;

    node_save($node);

    // Setup a menu item.
    $item = array(
      'link_title' => st($data['title']),
      'link_path' => drupal_get_normal_path($data['alias']),
      'menu_name' => 'main-menu',
      'weight' => $data['weight'],
    );
    menu_link_save($item);
  }
}

/**
 * Implements hook_update_N().
 */

/**
 * Add 1516 to course year field for all course content type nodes.
 */
function uw_ct_academic_calendar_update_7100() {
  // Check if the course content nodes in database.
  $course_node_count = db_query("SELECT COUNT(*) FROM {node} WHERE type = 'uw_graduate_course'")->fetchField();

  // When the course content nodes are exist in the database, update field_data_field_course_year to 1516.
  if ($course_node_count > 0 && db_table_exists('field_data_field_course_year')) {
    $query = "SELECT * FROM {field_data_field_course_id} WHERE bundle = 'uw_graduate_course'";
    $result = db_query($query);
    foreach ($result as $id => $record) {
      $data = array(
        'entity_type' => 'node',
        'bundle' => 'uw_graduate_course',
        'deleted' => $record->deleted,
        'entity_id' => $record->entity_id,
        'revision_id' => $record->revision_id,
        'language' => 'und',
        'delta' => $record->delta,
        'field_course_year_value' => '1516',
        'field_undergrad_award_desc_format' => $record->field_course_id_format,
      );
      drupal_write_record('field_data_field_course_year', $data);
    }
  }
}

/**
 * Update URL alias for all course content type nodes.
 *
 * Changes it from graduate-course/[title] to graduate-course/1516/[title].
 */
function uw_ct_academic_calendar_update_7101() {

  // Get the max pid from url_alias table.
  $pid = db_query("SELECT MAX(pid) FROM {url_alias}")->fetchField();

  // Get all graduate course nodes.
  $course_node_result = db_query("SELECT nid FROM {node} WHERE type = 'uw_graduate_course'");

  // Loop each course node, to change its alias from graduate-course/[title] to graduate-course/1516/[title].
  foreach ($course_node_result as $item) {

    $source = 'node/' . $item->nid;
    // Get pid and alias of course content type node.
    $query = "SELECT pid, alias FROM {url_alias} where source = :source";
    $args = array(
      ':source' => $source,
    );

    $course_url_alias_result = db_query($query, $args);

    foreach ($course_url_alias_result as $record) {
      $alias_array = explode("/", $record->alias);
      $alias_new = 'graduate-course/1516/' . $alias_array[1];
      $pid = $pid + 1;
      $data = array(
        'pid' => $pid,
        'source' => $source,
        'alias' => $alias_new,
        'language' => 'und',
      );
      drupal_write_record('url_alias', $data);
      db_delete('url_alias')->condition('pid', $record->pid)->execute();
    }
  }
}

/**
 * Delete field_gsac_term_year field.
 */
function uw_ct_academic_calendar_update_7102() {
  // Feature revert to remove field_gasc_term_year.
  features_revert(array('uw_ct_academic_calendar' => array('field_base', 'field_instance')));
  $field_gsac_term_year = field_info_instance('node', 'field_gsac_term_year', 'uw_academic_calendar');

  if (!empty($field_gsac_term_year)) {
    field_delete_field('field_gsac_term_year');
    drupal_set_message(t('field_gasc_term_year field has been successfully removedss.'), 'status');
  }
}

/**
 * Update pathauto paths for the existing graduate program nodes.
 */
function uw_ct_academic_calendar_update_7103(&$sandbox) {
  // Selecting only published nodes of graduate program content type that have term-year path, ordered by node ID.
  $query = "SELECT DISTINCT nid FROM {node} node LEFT JOIN {url_alias} url_alias ON url_alias.source = CONCAT('node/', nid) WHERE type IN ('uw_academic_calendar') AND status = 1 ORDER BY nid";
  if (!isset($sandbox['total'])) {
    $result = db_query($query);
    $sandbox['total'] = $result->rowCount();
    $sandbox['current'] = 0;
    // Revert the settings from this feature so we are using the right ones.
    features_revert(array('uw_ct_academic_calendar' => array('variable')));
  }

  // Only do something if there are records.
  if ($sandbox['total'] > 0) {
    // How many nodes should be processed per pass. The higher this number is, the faster your update will
    // complete, but the more likely your server will run out of memory or timeout.
    $nodes_per_pass = 50;

    // Get the nodes to process during this pass.
    $result = db_query_range($query, $sandbox['current'], $nodes_per_pass);
    while ($row = $result->fetchAssoc()) {
      // Load the node and look up its current alias.
      $node = node_load($row['nid']);
      $path_orig = drupal_lookup_path('alias', 'node/' . $node->nid);
      // Update the alias based on the new structure.
      pathauto_node_update_alias($node, 'update');
      $path_new = drupal_lookup_path('alias', 'node/' . $node->nid);
      // Increment "current" by 1.
      $sandbox['current']++;
    }

    // Set the value for finished. If current == total then finished will be 1, signifying we are done.
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
  else {
    $sandbox['#finished'] = 1;
  }
}

/**
 * Delete field_course_year.
 */
function uw_ct_academic_calendar_update_7104() {
  $fields_to_delete = array(
    'field_course_year',
  );
  foreach ($fields_to_delete as $field_name) {
    field_delete_field($field_name);
    watchdog('uw_ct_academic_calendar', 'Deleted the :field_name field from graduate course content type instances.', array(':field_name' => $field_name));
  }

  // The fields aren't really deleted until the purge function runs, ordinarily
  // during cron.  Count the number of fields we need to purge, and add five in
  // case a few other miscellaneous fields are in there somehow.
  field_purge_batch(count($fields_to_delete) + 2);

  // THEN, in case of field type change, revert the feature.
  features_revert(array('uw_ct_academic_calendar' => array('field')));
}

/**
 * Run _uw_ct_academic_calendar_create_landing_page().
 *
 * The landing page with alias graduate-course-search will create automatically
 * when uw_ct_academic_calendar module has been enabled, but the landing page is
 * not exist.
 */
function uw_ct_academic_calendar_update_7105() {
  if (module_exists('uw_ct_academic_calendar')) {
    _uw_ct_academic_calendar_create_landing_page();
  }
}

/**
 * Update course subject field if required.
 *
 * The oldest versions of the archive have the old text value and a
 * list_text field type is now used.  Updating the field only if required.
 */
function uw_ct_academic_calendar_update_7106() {

  // Get course subject fields that are text types.
  $field_course_subject = db_select('field_config', 'fc')
    ->fields('fc', array('id', 'type', 'module'))
    ->condition('fc.field_name', 'field_course_subject')
    ->condition('fc.type', 'text')
    ->execute()
    ->fetchAll();

  // If there are fields that are text type, convert to list_text.
  if (count($field_course_subject) > 0) {

    // Step through each field and convert to list_text.
    foreach ($field_course_subject as $fcs) {

      // Update field_config to use list_text for field_course_subject.
      $num_updated = db_update('field_config')
        ->fields(array(
          'type' => 'list_text',
          'module' => 'list',
        ))
        ->condition('id', $fcs->id)
        ->execute();
    }

    // Drop the format type of the course subject since we are switching
    // to list_text and not using text type anymore.
    db_drop_field('field_data_field_course_subject', 'field_course_subject_format');

    // Update all the course subject values to be blank.  We can do this
    // since any course subjects that were text before do not use the new features
    // where course subjects are list_text and now we can sucessfully feature-revert.
    $num_updated = db_update('field_data_field_course_subject')
      ->fields(array(
        'field_course_subject_value' => '',
      ))
      ->condition('bundle', 'uw_graduate_course')
      ->execute();
  }
}

/**
 * Change name of AHS to "Faculty of Health".
 */
function uw_ct_academic_calendar_update_7107() {
  // Delete item "Manage Faculty of Applied Health Sciences page" on
  // "menu-site-management".
  $links = menu_load_links('menu-site-management');
  foreach ($links as $link) {
    if ($link['link_title'] === 'Manage Faculty of Applied Health Sciences page') {
      menu_link_delete($link['mlid']);
    }
  }

  // Rename menu "Faculty of Applied Health Sciences".
  db_update('menu_links')
    ->fields([
      'link_title' => 'Faculty of Health',
    ])
    ->condition('link_title', 'Faculty of Applied Health Sciences')
    ->execute();
}
