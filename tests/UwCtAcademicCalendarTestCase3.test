<?php

/**
 * Test case 2: Access for "site manager", "administrator", "WCMS support".
 *
 * Able to access admin views.
 */
class UwCtAcademicCalendarTestCase3 extends UwWcmsTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => t('Graduate Studies Academic Calendar 3'),
      'description' => t('Test uw_ct_academic_calendar.'),
      'group' => t('UW Graduate Studies Academic Calendar'),
      'mode' => 'site',
    ];
  }

  /**
   * The tests.
   */
  public function test() {
    module_enable(['uw_ct_academic_calendar']);

    foreach ([
      'site manager',
      'administrator',
      'WCMS support',
    ] as $user) {
      if ($this->loggedInUser !== $this->users[$user]) {
        $this->drupalLogin($this->users[$user]);
      }

      foreach ([
        'admin/config/system/gasc_settings',
        'admin/config/system/graduate_program/graduate_program_search_header',
        'admin/config/system/graduate_program/graduate_program_ahs_block',
        'admin/config/system/graduate_program/graduate_program_arts_block',
        'admin/config/system/graduate_program/graduate_program_eng_block',
        'admin/config/system/graduate_program/graduate_program_env_block',
        'admin/config/system/graduate_program/graduate_program_math_block',
        'admin/config/system/graduate_program/graduate_program_sci_block',
        'admin/config/system/graduate_program/graduate_program_theo_block',
        'admin/graduate-program-report',
        'admin/graduate-program-report-csv',
        'admin/graduate-program-quick-report',
        'admin/graduate-program-quick-report-csv',
        'admin/graduate-program-mass-publish',
        'admin/graduate-course-report',
        'admin/graduate-course-report-csv',
        'admin/graduate-course-quick-report',
        'admin/graduate-course-quick-report-csv',
        'admin/structure/taxonomy/gsac_admit_term',
        'admin/structure/taxonomy/gsac_term_year',
        'admin/structure/taxonomy/gsac_application_materials',
        'admin/structure/taxonomy/gsac_delivery_mode',
        'admin/structure/taxonomy/gsac_program',
        'admin/structure/taxonomy/gsac_milestone_name',
        'admin/structure/taxonomy/gsac_program_type',
        'admin/structure/taxonomy/gsac_registration',
        'admin/structure/taxonomy/gsac_study',
        'admin/config/system/import_graduate_courses',
      ] as $path) {
        $this->drupalGet($path);
        $this->assertResponse(200, $user . ' user is able to access ' . $path . '.');
      }
    }
  }

}
