<?php

/**
 * @file
 * uw_ct_academic_calendar.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_academic_calendar_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_academic_calendar_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_academic_calendar_node_info() {
  $items = array(
    'uw_academic_calendar' => array(
      'name' => t('Graduate Program'),
      'base' => 'node_content',
      'description' => t('Graduate Studies Academic Calendar'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'uw_graduate_course' => array(
      'name' => t('Graduate Course'),
      'base' => 'node_content',
      'description' => t('Import graduate courses from Open API.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
