<?php

/**
 * @file
 * uw_ct_academic_calendar.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_academic_calendar_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gsac_admission_req|node|uw_academic_calendar|form';
  $field_group->group_name = 'group_gsac_admission_req';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_academic_calendar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Admission requirements',
    'weight' => '11',
    'children' => array(
      0 => 'field_gsac_application_materials',
      1 => 'field_gsac_elp',
      2 => 'field_gsac_materials_sup',
      3 => 'field_gsac_min_req',
      4 => 'field_gsac_references',
      5 => 'field_gsac_elp_need',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-gsac-admission-req field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_gsac_admission_req|node|uw_academic_calendar|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gsac_program_info|node|uw_academic_calendar|form';
  $field_group->group_name = 'group_gsac_program_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_academic_calendar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Program information',
    'weight' => '9',
    'children' => array(
      0 => 'field_gsac_addition_program',
      1 => 'field_gsac_admit_term',
      2 => 'field_gsac_delivery_mode',
      3 => 'field_gsac_dept_program',
      4 => 'field_gsac_faculty',
      5 => 'field_gsac_length_program',
      6 => 'field_gsac_length_tag',
      7 => 'field_gsac_program_type',
      8 => 'field_gsac_registration_option',
      9 => 'field_gsac_study_option',
      10 => 'field_gsac_delivery_info',
      11 => 'field_gsac_registration_info',
      12 => 'field_gsac_accelerated_detail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-gsac-program-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_gsac_program_info|node|uw_academic_calendar|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gsac_research_area|node|uw_academic_calendar|form';
  $field_group->group_name = 'group_gsac_research_area';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_academic_calendar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Areas of Research',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-gsac-research-area field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_gsac_research_area|node|uw_academic_calendar|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Admission requirements');
  t('Areas of Research');
  t('Program information');

  return $field_groups;
}
