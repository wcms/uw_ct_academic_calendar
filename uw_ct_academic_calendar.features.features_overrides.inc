<?php

/**
 * @file
 * uw_ct_academic_calendar.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_academic_calendar_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable.
  $overrides["variable.pathauto_max_component_length.value"] = 100;

  return $overrides;
}
