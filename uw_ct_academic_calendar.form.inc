<?php

/**
 * @file
 * uw_ct_academic_calendar.form.inc
 */

/**
 *
 */
function uw_ct_academic_calendar_import_form($form, &$form_state) {
  // This is the list of terms.  The term id is the key and the term name is the value.
  $startyear = 2017;
  $endyear = date('Y') + 2;
  $startid = 117;
  for ($year = $startyear; $year <= $endyear; $year++) {
    $terms[$startid . '1'] = 'Winter ' . $year;
    $terms[$startid . '5'] = 'Spring ' . $year;
    $terms[$startid . '9'] = 'Fall ' . $year;
    $startid++;
  }
  $limits[5] = '5';
  $limits[10] = '10';
  $limits[20] = '20';
  $limits[30] = '30';
  $limits[50] = '50';
  $limits[80] = '80';
  $limits[100] = '100';
  $limits[150] = '150';
  $limits[200] = '200';

  if (!isset($form_state['storage']['confirm'])) {
    $form['field_course_url'] = array(
      '#type' => 'select',
      '#title' => t('Please select term: '),
      '#default_value' => array('1175'),
      '#options' => $terms,
      '#required' => TRUE,
    );

    $form['field_course_batch_limit'] = array(
      '#type' => 'select',
      '#title' => t('Please select batch limit: '),
      '#default_value' => array('10'),
      '#options' => $limits,
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;
  }
  else {
    $message = "Are you sure you want to import " . $terms[$form_state['input']['field_course_url']] . "?";
    $description = "This will update all content currently on the Graduate Studies Calendar and remove any unused content.  This action can not be undone.";

    $form['field_course_url'] = array(
      "#type" => "hidden",
      "#title" => "course_url",
      "#value" => $form_state['input']['field_course_url'],
    );

    $form['field_course_batch_limit'] = array(
      "#type" => "hidden",
      "#title" => "batch_limit",
      "#value" => $form_state['input']['field_course_batch_limit'],
    );

    return confirm_form($form, $message, 'admin/config/system/import_graduate_courses', $description, 'Confirm', 'Cancel');
  }
}

/**
 *
 */
function uw_ct_academic_calendar_import_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    // These are the settings to use in the batch of the delete graduate courses.
    // start is for what record to start from, these will be 0, unless extreme circumstances.
    // limit is how many records we would like to process at a time, 20 looks about right to give users good feedback.
    $source_json_file = '';
    $source = '';
    $settings['limit'] = $form_state['input']['field_course_batch_limit'];
    $settings['course_url'] = $form_state['input']['field_course_url'];

    // This is the batch array from drupal core.
    $batch = array(
      'operations' => array(),
      'title' => t('Batch import graduate courses'),
      'init_message' => t('Import is starting ...'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('import has encountered an error.'),
      'finished' => '_uw_ct_academic_calendar_batch_import_finished',
    );

    $batch['operations'][] = array('_uw_ct_academic_calendar_batch_import_courses', array($settings));

    // Start the batch process.
    batch_set($batch);
  }
}
