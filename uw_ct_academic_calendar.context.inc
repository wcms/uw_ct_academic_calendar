<?php

/**
 * @file
 * uw_ct_academic_calendar.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_academic_calendar_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gasc_course_search';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'graduate-course-search' => 'graduate-course-search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-fb80f59199db79b6f58d7864bfd1fed9' => array(
          'module' => 'views',
          'delta' => 'fb80f59199db79b6f58d7864bfd1fed9',
          'region' => 'content',
          'weight' => '49',
        ),
        'views-1e646b1bcca7fd6b8407e3a7402d3e7c' => array(
          'module' => 'views',
          'delta' => '1e646b1bcca7fd6b8407e3a7402d3e7c',
          'region' => 'content',
          'weight' => '50',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gasc_course_search'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gasc_course_search_result';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'course-search-result' => 'course-search-result',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-fb80f59199db79b6f58d7864bfd1fed9' => array(
          'module' => 'views',
          'delta' => 'fb80f59199db79b6f58d7864bfd1fed9',
          'region' => 'content',
          'weight' => '-50',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gasc_course_search_result'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_ahs_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'applied-health-sciences' => 'applied-health-sciences',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-ahs_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'ahs_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_ahs_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_arts_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'arts' => 'arts',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-ahs_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'ahs_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_arts_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_eng_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'engineering' => 'engineering',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-eng_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'eng_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_eng_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_env_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'environment' => 'environment',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-env_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'env_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_env_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_math_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'mathematics' => 'mathematics',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-math_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'math_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_math_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_sci_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'fscience' => 'science',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-sci_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'sci_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_sci_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'gsac_theo_block';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'theology' => 'theology',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_academic_calendar-theo_block' => array(
          'module' => 'uw_ct_academic_calendar',
          'delta' => 'theo_block',
          'region' => 'content',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['gsac_theo_block'] = $context;

  return $export;
}
